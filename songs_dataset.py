# songs_dataset.py
import json
import random
# Dataset: Top Hits Spotify from 2000-2019
def get_music_data():
    jsonFile = open('songs_data.json', encoding='utf-8')
    data = json.load(jsonFile)
    return data

def get_artists():
    artists = []
    for songData in get_music_data():
        artist = songData['artist'].lower()
        if artist not in artists:
            artists.append(artist)
    return artists

def artist_exists(artist):
    return artist in get_artists()

def get_artist_from_input(input_artist):
    for artist in get_artists():
        if (" " + input_artist.lower() + " ") in (" " + artist.lower() + " "):
            return artist
    return None

def get_songs_names():
    songs_names = []
    for songData in get_music_data():
        song_name = songData['song'].lower()
        if song_name not in songs_names:
            songs_names.append(song_name)
    return songs_names

def get_artists_with_repetition():
    artists = []
    for songData in get_music_data():
        artist = songData['artist'].lower()
        artists.append([artist])
    return artists

def get_songs_names_with_repetition():
    songs_names = []
    for songData in get_music_data():
        song_name = songData['song'].lower()
        songs_names.append([song_name])

    return songs_names

def get_artist_songs_by_name(artist_name):
    songs = []
    for songData in get_music_data():
        if songData['artist'].lower() == artist_name.lower():  
            songName = songData['song'].lower()
            songs.append(songName)
            
    if len(songs) == 0:
        return "Ups! No encuentro canciones para el artista especificado"
    
    return songs

# Returns the 15 most popular songs from 2000-2019
def get_most_popular_songs():
    most_popular_songs = []
    for song in sort_songs_by_popularity()[:15]:
        most_popular_songs.append({'artist': song['artist'].lower(), 'song': song['song'].lower(), 'popularity': song['popularity']})
    
    return most_popular_songs
               
def sort_songs_by_popularity():
    return sorted(get_music_data(), key=lambda song:song['popularity'], reverse=True)

def get_popularity_of_song(song_object):
    return song_object['popularity']

def sort_songs_by_danceability():
    return sorted(get_music_data(), key=lambda song:song['danceability'], reverse=True)

def get_most_danceable_songs():
    most_danceable_songs = []
    for song in sort_songs_by_danceability()[:15]:
        most_danceable_songs.append({'artist': song['artist'].lower(), 'song': song['song'].lower(), 'danceability': song['danceability']})
        
    return most_danceable_songs

def get_song_years():
    years = []
    for song_data in get_music_data():
        if song_data['year'] not in years:
            years.append(song_data['year'])
    years.sort()
    return years

def get_song_genres():
    genres = []
    for song_data in get_music_data():
        if song_data['genre'] not in genres:
            genres.append(song_data['genre'])
    return genres

def parse_genre(genre):
    genres_obtained = []
    genres_splitted_by_coma = []
    genres_splitted_by_dash = []

    if ',' in genre:
        genres_splitted_by_coma = genre.split(',')
        for elem in genres_splitted_by_coma:
            if '/' in elem:
                genres_splitted_by_dash = elem.split('/')
                genres_obtained.extend(genres_splitted_by_dash)
            else:
                genres_obtained.append(elem)
    else:
        return genre
                
    return genres_obtained
    
             
def get_genre_from_input(input_genre):
    for genre in get_song_genres():
        genres = parse_genre(genre)
        if len(genres) > 1:
            for elem in genres:
                if (" " + input_genre.lower() + " ") in (" " + elem.lower() + " "):
                    return genre
        else:
            if (" " + input_genre.lower() + " ") in (" " + elem.lower() + " "):
                return genre
    
    return None


def exists_song_with_given_year(year):
    return year in get_song_years()

def get_random_song_by_year(song_year):
    data = get_music_data()
    random.shuffle(data)
    song = None
    for songData in data:
        if songData['year'] == int(song_year):  
            song = {'artist': songData['artist'], 'song': songData['song'], 'year': songData['year']}
            break
    return song

def get_random_song_by_genre(genre):
    data = get_music_data()
    random.shuffle(data)
    song = None
    for songData in data:
        if songData['genre'].lower() == genre.lower():  
            song = (songData['artist'], songData['song'], songData['genre'])
            break
        
    return {'artist': song[0], 'song': song[1], 'genre': song[2]}
        
        
    


    
    
    
        