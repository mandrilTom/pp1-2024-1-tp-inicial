# fingerprint_auth.py
import os
#from aiohttp import Fingerprint
import cv2
import json
import glob
import random

def get_user_data():
    jsonFile = open('usuarios.json', encoding='utf-8')
    data = json.load(jsonFile)
    return data

def get_user_directory_path(user):
    for userData in get_user_data():
        userName = userData['usuario'].lower()
        if(user.lower() == userName):
            return userData['huella']
    return ""

def validUser(user):
    for userData in get_user_data():
        userName = userData['usuario'].lower()
        if(user.lower() == userName):
            return True
    return False

def get_random_fingerprint_from_sample(fingerprint_path):
    random_fingerprint_file_path = random.choice(os.listdir(fingerprint_path))
    fingerprint = fingerprint_path + "/" + random_fingerprint_file_path
    
    return fingerprint

def get_fingerprint_samples_from_id(fingerprint_id):
    fingerprint_path = None
    if fingerprint_id == '1':
        fingerprint_path = "fingerprint_samples/gabriel_Alt"
    elif fingerprint_id == '2':
        fingerprint_path = "fingerprint_samples/mariano_Alt"
    elif fingerprint_id == '3':
        fingerprint_path = "fingerprint_samples/tomas_Alt"
    
    return fingerprint_path
   
def auth_with_fingerprint(user, data_huella):
    if(validUser(user) == False):
        return False
    sample = cv2.imread(data_huella)

    sift = cv2.SIFT.create()
    match_points = []
    dataset = get_user_directory_path(user)
    for file in glob.glob(dataset+"/*.BMP"):        
        fingerprint_img = cv2.imread(file)
        _, descriptors_sample = sift.detectAndCompute(sample, None)
        _, descriptors_current_fingerprint = sift.detectAndCompute(fingerprint_img, None)
        matches = cv2.FlannBasedMatcher({"algorithm": 1, "trees": 10}, {}).knnMatch(descriptors_sample, descriptors_current_fingerprint, k=2)

        for p, q in matches:
            if p.distance < 0.1 * q.distance:
                match_points.append(p)
    
    return len(match_points) != 0