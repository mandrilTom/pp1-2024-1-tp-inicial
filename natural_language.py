# natural_language.py
import nltk
nltk.download('punkt')
nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
import json
import pickle

import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import SGD
import random


def trainModel():
    words, classes, documents = generate_nlp_data()
    train_x, train_y = generate_training_data(words, classes, documents)
    create_and_train_model(train_x, train_y)

def generate_nlp_data():
    words=[]
    classes = []
    documents = []
    ignore_words = ['?', '!']
    data_file = open('intents.json').read()
    intents = json.loads(data_file)

    # Por cada intent (intencion usuario) tomamos los patrones y los tokenizamos (descomponemos la oracion en palabras)
    # Agregamos una tupla por cada conjunto de palabras al array documents, la tupla contiene la descomposicion en palabras del pattern y a que tag pertenece
    for intent in intents['intents']:
        for pattern in intent['patterns']:
            w = nltk.word_tokenize(pattern)
            words.extend(w)

            documents.append((w, intent['tag']))

            # Agregamos los tags a nuestra lista de clases/tags (si no pertenecen aun)
            if intent['tag'] not in classes:
                classes.append(intent['tag'])

    # La lematizacion consiste en reducir una palabra a su 'lema', es decir, a la forma mas simple de la misma tal que conserve su significado
    # Lematizamos cada palabra extraida de los patterns menos aquellas que nos interesa ignorar
    words = [lemmatizer.lemmatize(w.lower()) for w in words if w not in ignore_words]
    words = sorted(list(set(words)))

    classes = sorted(list(set(classes)))

    print (len(documents), "documents")

    print (len(classes), "classes", classes)

    print (len(words), "unique lemmatized words", words)


    pickle.dump(words,open('words.pkl','wb'))
    pickle.dump(classes,open('classes.pkl','wb'))
    return words,classes,documents

def generate_training_data(words, classes, documents):
    # Inicializamos la data para el entrenamiento del modelo IA
    training = []
    output_empty = [0] * len(classes)

    for doc in documents:
        # doc[0] = lista de palabras obtenidas al tokenizar el pattern (doc)
        # doc[1] = tag al cual pertenece el pattern (doc)
        
        # Inicializamos la 'bag of words', que es una coleccion de palabras que representan una oración, que incluye un conteo de palabras
        bag = []
        # Lista de palabras tokenizadas de dicho pattern
        pattern_words = doc[0]
        # Lematizamos cada palabra de nuevo, con el proposito de representar palabras relacionadas
        pattern_words = [lemmatizer.lemmatize(word.lower()) for word in pattern_words]
        #  Agregamos a nuestro array de bag of words un 1 si la palabra matchea con nuestro pattern actual. De lo contrario, agregamos un 0 para decir que no matchea
        for w in words:
            bag.append(1) if w in pattern_words else bag.append(0)

        # Por cada pattern recorrido, Output devolverá un 0 por cada tag que no sea el actual o un 1 para el tag actual
        output_row = list(output_empty)
        output_row[classes.index(doc[1])] = 1
        training.append([bag, output_row])

    random.shuffle(training)
    training = np.array(training, dtype=object)
    # create train and test lists. X - patterns, Y - intents
    train_x = list(training[:,0])
    train_y = list(training[:,1])
    print("Training data created")
    return train_x, train_y
    
def create_and_train_model(train_x, train_y):
    # Create model - 3 layers. First layer 128 neurons, second layer 64 neurons and 3rd output layer contains number of neurons
    # equal to number of intents to predict output intent with softmax
    model = Sequential()
    model.add(Dense(128, input_shape=(len(train_x[0]),), activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(train_y[0]), activation='softmax'))

    # Compile model. Stochastic gradient descent with Nesterov accelerated gradient gives good results for this model
    sgd = SGD(learning_rate=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

    #fitting and saving the model
    hist = model.fit(np.array(train_x), np.array(train_y), epochs=200, batch_size=5, verbose=1)
    model.save('chatbot_model.h5', hist)

    print("model created")