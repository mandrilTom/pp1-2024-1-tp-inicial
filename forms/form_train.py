import tkinter as tk
from tkinter import ttk, messagebox
from tkinter.font import BOLD
import util.generic as utl
from forms.form_chat import ChatAplication 
from natural_language import trainModel

class train:
    
    entrenado = False
    
    def chatbot(self):
        self.ventana.destroy()
        ChatAplication()
        
    def entrenar(self):
        trainModel()
        self.entrenado = True
        messagebox.showinfo(message="El bot fue entrenado con exito",title="Mensaje")  
    
    def go_back(self):
        self.ventana.destroy()
        from forms.home import App
        App()      
     
                      
    def __init__(self):        
        self.ventana = tk.Tk()                             
        self.ventana.title('Menu')
        self.ventana.geometry('800x500')
        self.ventana.config(bg='#fcfcfc')
        self.ventana.resizable(width=0, height=0)    
        utl.centrar_ventana(self.ventana,800,500)
        
        logo =utl.leer_imagen("./imagenes/music.png", (200, 200))
        # frame_logo
        frame_logo = tk.Frame(self.ventana, bd=0, width=300, relief=tk.SOLID, padx=10, pady=10,bg='#3a7ff6')
        frame_logo.pack(side="left",expand=tk.YES,fill=tk.BOTH)
        label = tk.Label( frame_logo, image=logo,bg='#4d9fd6' )
        label.place(x=0,y=0,relwidth=1, relheight=1)
        
        #frame_form
        frame_form = tk.Frame(self.ventana, bd=0, relief=tk.SOLID, bg='#fcfcfc')
        frame_form.pack(side="right",expand=tk.YES,fill=tk.BOTH)
        #frame_form
        
        #frame_form_top
        frame_form_top = tk.Frame(frame_form,height = 50, bd=0, relief=tk.SOLID,bg='black')
        frame_form_top.pack(side="top",fill=tk.X)
        title = tk.Label(frame_form_top, text="Menu",font=('Times', 30), fg="#666a88",bg='#fcfcfc',pady=50)
        title.pack(expand=tk.YES,fill=tk.BOTH)
        #end frame_form_top

        #frame_form_fill
        frame_form_fill = tk.Frame(frame_form,height = 50,  bd=0, relief=tk.SOLID,bg='#fcfcfc')
        frame_form_fill.pack(side="bottom",expand=tk.YES,fill=tk.BOTH)

        chat_bot = tk.Button(frame_form_fill,text="Chatbot",font=('Times', 15,BOLD),bg='#3a7ff6', bd=0,fg="#fff",command=self.chatbot)
        chat_bot.pack(fill=tk.X, padx=20,pady=20)        
        chat_bot.bind("<Return>", (lambda event: self.chatbot()))
        #end frame_form_fill
        
        
        entrenamiento = tk.Button(frame_form_fill,text="Entrenar Chatbot",font=('Times', 15,BOLD),bg='#3a7ff6', bd=0,fg="#fff",command=self.entrenar)
        entrenamiento.pack(fill=tk.X, padx=20,pady=20)        
        entrenamiento.bind("<Return>", (lambda event: self.entrenar()))
        #end frame_form_fill
        
        retroceder = tk.Button(frame_form_fill,text="retroceder",font=('Times', 15,BOLD),bg='#3a7ff6', bd=0,fg="#fff",command=self.go_back)
        retroceder.pack(fill=tk.X, padx=75,pady=75)        
        retroceder.bind("<Return>", (lambda event: self.go_back()))
        
        self.ventana.mainloop()

        