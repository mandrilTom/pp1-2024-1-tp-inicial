import tkinter as tk
from tkinter import ttk, messagebox
from tkinter.font import BOLD
from tkinter import filedialog
import util.generic as utl
from forms.form_train import train
from fingerprint_auth import *



class Login:
    huella = ""  
    
    def verificar(self):
        usu = self.usuario.get()  
        if(self.huella != "" and usu != ""):
            if(auth_with_fingerprint(usu, self.huella)):
                self.ventana.destroy()
                train()
            else:
                messagebox.showerror(message="El usuario o contraseñas son incorrectos",title="Mensaje") 
        else:
            messagebox.showerror(message="No lleno toda la informacion",title="Mensaje") 
            
    def go_back(self):
        self.ventana.destroy()
        from forms.home import App
        App()
            
    def obtenerImagen(self):
        filepath = tk.filedialog.askopenfilename(filetypes=[('Imegen huella', '*.BMP')]) 
        self.huella = filepath  
                      
    def __init__(self):      
        self.ventana = tk.Tk()                             
        self.ventana.title('Inicio de sesion')
        self.ventana.geometry('800x500')
        self.ventana.config(bg='#fcfcfc')
        self.ventana.resizable(width=0, height=0)    
        utl.centrar_ventana(self.ventana,800,500)
        
        logo =utl.leer_imagen("./imagenes/music.png", (200, 200))
        # frame_logo
        frame_logo = tk.Frame(self.ventana, bd=0, width=300, relief=tk.SOLID, padx=10, pady=10,bg='#3a7ff6')
        frame_logo.pack(side="left",expand=tk.YES,fill=tk.BOTH)
        label = tk.Label( frame_logo, image=logo,bg='#4d9fd6' )
        label.place(x=0,y=0,relwidth=1, relheight=1)
        
        #frame_form
        frame_form = tk.Frame(self.ventana, bd=0, relief=tk.SOLID, bg='#fcfcfc')
        frame_form.pack(side="right",expand=tk.YES,fill=tk.BOTH)
        #frame_form
        
        #frame_form_top
        frame_form_top = tk.Frame(frame_form,height = 50, bd=0, relief=tk.SOLID,bg='black')
        frame_form_top.pack(side="top",fill=tk.X)
        title = tk.Label(frame_form_top, text="Inicio de sesion",font=('Times', 30), fg="#666a88",bg='#fcfcfc',pady=50)
        title.pack(expand=tk.YES,fill=tk.BOTH)
        #end frame_form_top

        #frame_form_fill
        frame_form_fill = tk.Frame(frame_form,height = 50,  bd=0, relief=tk.SOLID,bg='#fcfcfc')
        frame_form_fill.pack(side="bottom",expand=tk.YES,fill=tk.BOTH)

        etiqueta_usuario = tk.Label(frame_form_fill, text="Usuario", font=('Times', 14) ,fg="#666a88",bg='#fcfcfc', anchor="w")
        etiqueta_usuario.pack(fill=tk.X, padx=20,pady=5)
        self.usuario = ttk.Entry(frame_form_fill, font=('Times', 14))
        self.usuario.pack(fill=tk.X, padx=20,pady=10)

        huellapath = tk.Button(text="Huella", font=('Times', 14),fg="#666a88",bg='#fcfcfc' , anchor="w", command= self.obtenerImagen)
        huellapath.pack(fill=tk.X, padx=25,pady=200)   
        huellapath.pack()

        inicio = tk.Button(frame_form_fill,text="Iniciar sesion",font=('Times', 15,BOLD),bg='#3a7ff6', bd=0,fg="#fff",command=self.verificar)
        inicio.pack(fill=tk.X, padx=20,pady=20)        
        inicio.bind("<Return>", (lambda event: self.verificar()))
        #end frame_form_fill
        retroceder = tk.Button(frame_form_fill,text="retroceder",font=('Times', 15,BOLD),bg='#3a7ff6', bd=0,fg="#fff",command=self.go_back)
        retroceder.pack(fill=tk.X, padx=75,pady=75)        
        retroceder.bind("<Return>", (lambda event: self.go_back()))
        
        self.ventana.mainloop()