from tkinter import *
import tkinter as tk
from tkinter.font import BOLD
import util.generic as utl
from chat import chatbot_response
from chat import get_chat_name


TEXT_COLOR = "#EAECEE"


class ChatAplication:
    
    
    def on_enter_pressed(self, event):
        msg = self.msg_entry.get()
        self._insert_message(msg, "Yo")
    
    def _insert_message(self, msg, sender):
        if not msg:
            return
    
        self.msg_entry.delete(0, END)
        msg1 = f"{sender}:{msg}\n\n"
        self.text_widget.config(state=NORMAL)
        self.text_widget.insert(END, msg1)
        self.text_widget.config(state=DISABLED)
    
        response = chatbot_response(msg)
        chatbot_msg = response[0]
        response_data = response[1]
        msg_displayed = chatbot_msg + '\n' + str(response_data)
        msg2 = f"{get_chat_name()}:{msg_displayed}\n\n"
        self.text_widget.config(state=NORMAL)
        self.text_widget.insert(END, msg2)
        self.text_widget.config(state=DISABLED)
        self.text_widget.see(END)
    
          
        
    def __init__(self):
        self.ventana = tk.Tk()                             
        self.ventana.title('Chatbot')
        self.ventana.geometry('800x500')
        self.ventana.config(bg='#fcfcfc')
        self.ventana.resizable(width=0, height=0)    
        utl.centrar_ventana(self.ventana,800,500)
    
        head_label = Label(self.ventana, bg= '#3a7ff6', fg="#fff",
                        text="Escribe algo y el chat bot te contestara", font=('Times', 18), pady=10)
        head_label.place(relwidth=1)
    
        line = Label(self.ventana, width = 450, bg='#fcfcfc')
        line.place(relwidth=1, rely=0.07, relheight=0.012)
    
        #ZONA DE CHAT
        self.text_widget = Text(self.ventana, width=20, height=2, fg="#666a88",bg='#b0cbfb',
                            font=('Times', 15,BOLD), padx=5, pady=5)
    
        self.text_widget.place(relheight=0.745, relwidth=1, rely=0.08)
        self.text_widget.config(state=DISABLED)
    
        scrollbar = Scrollbar(self.text_widget)
        scrollbar.place(relheight=1, relx=0.974)
        scrollbar.configure(command=self.text_widget.yview)
    
        #ZONA DE Usuario
        bottom_label = Label(self.ventana, bg='#3a7ff6', height=80)
        bottom_label.place(relwidth=1, rely=0.825)
    
        self.msg_entry = Entry(bottom_label, bg='#3a7ff6', fg=TEXT_COLOR, font=('Times', 15,BOLD))
        self.msg_entry.place(relwidth=0.74, relheight=0.06, rely=0.008, relx=0.011)
        self.msg_entry.focus()
        self.msg_entry.bind("<Return>", self.on_enter_pressed)
    
        enviar_boton = tk.Button(bottom_label, text="enviar", font=('Times', 15,BOLD), width=20, bg='#173262', bd=0,fg="#fff",
                         command=lambda: self.on_enter_pressed(None))
        enviar_boton.place(relx=0.77, rely=0.008, relheight=0.06, relwidth=0.22)
        
        self.ventana.mainloop()
    
    