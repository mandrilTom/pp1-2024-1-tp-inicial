import flask
from flask import request
from fingerprint_auth import *
from functools import wraps
app = flask.Flask(__name__)
app.config["DEBUG"] = True
from chat import chatbot_response

def build_json_error_response(errTitle, detail, statusCode):
    return {"error": errTitle, "detalle": detail}, statusCode

   
def check_auth_credentials(username, password):
    if username == '' or password == '':
        return False
    
    if (username.lower() == 'mariano' or username.lower() == 'gabriel' or username.lower() == 'tomas') and password == 'admin123':
        return True
    
    return False

    
def requires_basic_auth(f):
    @wraps(f)
    def auth(*args, **kwargs):
        username = request.headers.get('Usuario')
        fingerprint_id = request.headers.get('Id-huella')
        
        if username == '' or fingerprint_id == '':
            return  build_json_error_response("Error al autorizar", "El encabezado Usuario y el encabezado Huella no deben ir vacíos. Especifique su usario y numero de huella, intente nuevamente", 401)
        if not validUser(username):
            return build_json_error_response("Error al autorizar", "El usuario ingresado no existe", 401)
        fingerprint_sample_path = get_fingerprint_samples_from_id(fingerprint_id)
        
        if fingerprint_sample_path == None:
            return build_json_error_response("Error al autorizar", "El id huella ingresado no corresponse con alguno existente. Debe ser 1, 2 o 3", 401)

        random_fingerprint_path = get_random_fingerprint_from_sample(fingerprint_sample_path)
        valid = auth_with_fingerprint(username, random_fingerprint_path)
        
        if not valid:
            return build_json_error_response("Error al autorizar", "La huella ingresada no es válida para el usuario " + username, 401)
        return f(*args, **kwargs)
    return auth

@app.route('/api/chatbot/talk', methods=['POST'])
@requires_basic_auth
def return_chatbot_response():
    json_data = request.get_json()
    user_input = json_data.get("chatbot-input")
    
    if json_data == '' or user_input == '':
        return build_json_error_response("Error en el pedido", "Ingrese una instrucción para el chatbot no vacía", 400)
    
    response = chatbot_response(user_input)
    msg_chatbot = response[0]
    data_response = response[1]
    
    return {"musichat-response": msg_chatbot, "data": data_response}, 200


app.run()