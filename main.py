# main.py
import keyboard
from chat import chatbot_response
from fingerprint_auth import *
from natural_language import trainModel
nombre_chatbot = 'Musichat'

def chatbot():
    print("-------------------------------- Chatbot de música --------------------------------")
    while True:
        print(nombre_chatbot +": Hola! Soy un chatbot de musica, preguntame algo")
        userInput = input("Yo: ")
        response = chatbot_response(userInput)
        if response == 'exit':
            break
        print(nombre_chatbot + ": " + response[0] + "\n" + str(response[1]))
        continue
        
def runChatbot():
    print("-------------------------------- Menú chatbot de música --------------------------------")
    while True:
        print(nombre_chatbot +": Hola! Selecciona una opción, o cualquier otra tecla para salir del programa")
        print("1. Entrenar modelo IA")
        print("2. Usar chatbot")
        print("3. Menu anterior")

        key_pressed = keyboard.read_key()
        if key_pressed == '1':
            trainModel()
            continue
        elif key_pressed == '2':
            chatbot()
            break
        elif key_pressed == '3':
            runMenu()
        break
        
def runMenu():
    print(nombre_chatbot +": Hola bienvenido al inicio del chatbot de musica!")
    print("Ingrese su nombre de usuario")
    while True:
        usuario= input("Yo: ")
        fingerprint_path = None
        if validUser(usuario):
            print('Usuario Correcto')
            
            print("---------- Simulacion de ingreso de huella --------")
            print(nombre_chatbot +': Seleccione una huella a ingresar')
            print("1. Gabriel")
            print("2. Mariano")
            print("3. Tomas")
            
            key_pressed = keyboard.read_key()
            fingerprint_path = get_fingerprint_samples_from_id(key_pressed)
            if fingerprint_path == None:
                print(nombre_chatbot +': Respuesta invalida, saliendo del programa')
                break
            
            random_fingerprint_path = get_random_fingerprint_from_sample(fingerprint_path)
            valid = auth_with_fingerprint(usuario, random_fingerprint_path)
            
            if valid:
                print(nombre_chatbot +': Autenticacion con huella exitosa!')
                runChatbot()
            else:
                print(nombre_chatbot +': La validación de la huella no fue válida, intente nuevamente')
        else: 
                print(nombre_chatbot +': Usuario Incorrecto')
        print(nombre_chatbot +': Apreta 1 para salir')
        key_pressed = keyboard.read_key()
        if key_pressed == '1':
            break

runMenu()