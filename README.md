# Proyecto profesional I

# TP Inicial – Aplicación de la Inteligencia Artificial

## Integrantes

* Gabriel Adrian Fariña Fernandes
* Mariano Chun
* Tomas Eduardo Esteban Mandril

## Docentes

* Juan Carlos Monteros
* Evelin Emilce Aragon

## Enunciado TP Inicial
Después de leer el material e investigar respecto a la aplicación de algoritmos de Machine Learning para Inteligencia Artificial utilizando el lenguaje Python para el procesamiento de los datos, se pide:

    1. Documentar la investigación de los temas propuestos y subirla a un repositorio de trabajo

    2. Crear un Chatbot

    3. Lograr que el login se realice con reconocimiento de la huella digital en el celular

    4. Optimizar el Chatbot


## Objetivos del TP
* Investigar la aplicación de la IA en diferentes procesos de la vida diaria, a nivel personal, profesional y
empresarial.
* Profundizar en el conocimiento de:
  -  Onbording Digital
  -  Importancia de la IA en el onboarding
  -  Proceso de Onbording Digital
      *  Análisis de un documento de identidad
      *  Biometría
      *  Listas AML (Anti Money Laundering)
  -  Normativas existentes
* Mostrar en un Chatbot dicha aplicación

## Tipos de Chatbot
- Dumb chatbots
- Chatbot con tecnología word-spotting
- Chatbot con Inteligencia Artificial
- Según el tipo de interacción

## Consignas

### Entrega 19/3
* Entregar la evidencia del repositorio en el que trabajarán
* Documentar la Investigación realizada acerca de Aplicación de la IA, Onbording Digital, Normativas
existentes
* ¿Cuál es el objetivo u objetivos que desean lograr con el Chatbot?
* Herramientas a utilizar

### Resolución
<u>Documentación de la investigación</u>

#### Onboarding digital
*¿Qué es?* 

El onboarding digital no es más que un proceso de alta de clientes tradicional pero que además verifica la identidad real de un usuario, es decir, de verificar que el usuario es quien dice ser. Generalmente se suele dar en el segmento financiero para reducir el fraude, lavado de dinero y financiamento del terrorismo, entre otras cosas. El mecanismo de alta usualmente consiste en registrar los datos biometricos de la persona, para luego validarlos con un ente regulador, esta validación puede realizarse registrando una huella digital, uso de reconocimiento facial y/o escaneo de documento nacional de identidad.

*Importancia de la IA en el onboarding*

El digital onboarding se refiere al proceso de orientar y guiar a los clientes a través de su primera interacción con un producto o servicio de manera remota, utilizando tecnologías avanzadas como la inteligencia artificial (IA). La tecnología IA juega un papel fundamental en el onboarding digital al permitir la automatización de procesos de contratación de las empresas, la mejora de la eficiencia operativa y la reducción de errores humanos. A continuación se enumeran algunos beneficios que resaltan la importancia de la IA en este proceso de onboarding:

* **Mejora la experiencia del usuario**: La IA puede identificar productos y servicios adicionales a ofrecer al usuario que podrian beneficiarlo, en base a los datos que este proporciona en el proceso de onboarding.

* **Mejora el análisis de datos**: Durante el proceso de onboarding, la IA en segundo plano puede analizar millones de datos a traves de cientos de instituciones financieras y relacionarlos de tal forma para mejorar la identificación del aplicante, obtener un rating crediticio y ayudar a la identificación de maniobras de lavado de dinero, robos de identidad o ataques de bots.

*Proceso de Onbording Digital*

* **Análisis de un documento de identidad**: Hoy en día gracias a la IA, puede realizarse de forma mas rapida e inteligente la captura de datos de un DNI. Con el reconocimiento óptico de caracteres (OCR) y la IA, por ejemplo, se pueden leer todo tipo de documentos que contengan texto y los datos pueden extraerse automáticamente, sin necesidad de un proceso manual de lectura y comprobación de documentos. Esto se realiza a partir de una serie de pasos

  1. **Ingreso del documento**: Primero, el cliente debe ingresar o sacar una foto, o subir un archivo PDF del DNI. El software recortara la imagen y optimizará la calidad de imagen.
  2. **Reconocimiento de texto**: El software de OCR analiza los patrones de luz y oscuridad que componen las letras y los números para transformar la imagen escaneada en texto. El software OCR extrae todos los datos necesarios en cuestión de segundos y en este paso puede identificar duplicados, errores o fraudes.
  3. **Procesamiento a datos estructurados**: El texto obtenido se pasa a formato JSON u otro formato, listo para ser procesado, por ejemplo, para agregarlo a una base de datos.
  4. **Extracción de firma y foto**: Para mejorar la seguridad, se puede extraer la foto y la firma del cliente, en un formato utilizable. Esto se realiza por medio de computer vision y deep learning para analizar el documento sección por sección y dividirlo en texto y componentes visuales. Luego se comprueba que componentes pueden llegar a ser una firma o foto. En caso de identificarlos, el software recorta la firma y/o foto, convirtiendolo a un formato utilizable, lo que mas adelante puede utilizarse para verificaciones de identidad por medio de selfies o comparación de firmas.


* **Biometría**: La biometría incluye las medidas biológicas, o características físicas, que se pueden utilizar para identificar a las personas, por ejemplo, la clasificación de huellas dactilares, el reconocimiento facial y los exámenes de retina son todas formas de tecnología biométrica, por mencionar algunas. En el proceso de onboarding digital, el uso de la biometría se ha convetido en uno de los mecanismos mas utilizados para validar la identidad de un usuario. Como se mencionó, uno de los métodos mas usados es el de reconocimiento facial por medio de 'selfies', gracias a que hoy en día mucha gente cuenta con smartphones, cuyas camaras de alta resolución permiten realizar esta comprobación de identidad de forma correcta y rapida.

* **Listas AML (Anti Money Laundering)**: AML es la sigla de Anti-Money Laundering o sea la Prevención del Lavado de Dinero, un sistema de prevención esencial para las actividades vinculadas tanto a las finanzas como al cumplimiento de las empresas. Son procesos estandarizados e incorporados a la legislación de los países que pueden dar la garantía de que los clientes de una empresa no están realizando lavado de dinero. Los siguiente métodos son utilizados en el proceso de onboarding digital para un chequeo efectivo de AML:

  1. **PEP (Persona expuesta politicamente)**: Enfocado en detectar clientes de alto riesgo que tienen acceso a medios ilegales para obtener dinero como podrian ser sobornos o lavado de dinero a diferencia de los clientes comunes.
  2. **RCA (Familiares y socios cercanos)**: Permite monitorear personas o empresas cuya relación es cercana a alguna PEP, lo que permite identificar cualquier riesgo de delitos financieros debido a la conexión con la PEP.
  3. **Filtro de listas negras**: Enfocado en identificar si las personas pertenecen a algun país presente en las listas negras de paises creada por el GAFI (Grupo de Acción Financiera Internacional), lista la cual forman aquellos paises que no cumplen con los estandares de anti lavado de dinero y anti financiamiento del terrorismo.
  4. **Escaneo de medios adversos**: Mecanismo por el cual se examina si el cliente cuenta con reputación adversa, al buscar antecedentes penales del mismo o si esta asociado a ellos.
  5. **Monitoreo de transacciones en la cuenta**: Enfocado en mantener el rastro de las transacciones, depositos y retiros del cliente para prevenir maniobras delictivas. 

#### Normativas

### Onboarding Digital
  Las directrices sobre onboarding digital de la EBA(European Banking Authority) aplicables a las entidades del sector financiero son seis. Estas medidas hacen referencia a las políticas y a los procedimientos internos, al análisis de la solución, a la recopilación de información, a la coincidencia de la identidad del cliente, a la externalización y a los riesgos tecnológicos y de seguridad.
A continuación, se abordan cada una de ellas con el objetivo de conocer su alcance y facilitar su comprensión.

Políticas y procedimientos internos
Esta directriz persigue que las instituciones establezcan y mantengan políticas y procedimientos que tengan en cuenta el riesgo asociado a los procesos de incorporación del usuario en remoto y que al menos:

  1. Describan de manera genérica la solución de Onboarding Digital que se va a utilizar. A su vez, que se específiquen sus características y su modo de funcionamiento.
  2. Determinen las situaciones en las que se va a utilizar, la categoría de clientes y los productos y servicios a los que se va a aplicar.
  3. Especifiquen los pasos que están automatizados y los que no.
  4. Establezcan controles para asegurar que la primera transacción que va a realizar un nuevo cliente solo comience cuando se hayan aplicado medidas iniciales de DDC.
  5. Recojan programas de formación continua. De este modo, los empleados podrán conocer el funcionamiento de la solución y las herramientas que necesitan para responder ante posibles riesgos. Por ejemplo, en los casos en los que se debe aplicar la video identificación con revisión del proceso por parte de agentes para cumplir con el SEPBLAC, que esos agentes reciban la formación específica para llevar a cabo este examen.  

### Inteligencia Artificial

En Argentina no se tiene todavia una legislacion sobre inteligencia artificial lo que existe son recomendaciones para el uso de inteligencia artificial las cuales son:

  1. Proporcionalidad e inocuidad. En caso de que pueda producirse cualquier daño para los seres humanos, debe garantizarse la aplicación de procedimientos de evaluación de riesgos y la adopción de medidas para impedir que ese daño se produzca.

  2. Seguridad y protección. Los daños no deseados (riesgos de seguridad) y las vulnerabilidades a los ataques (riesgos de protección) deben ser evitados y deben tenerse en cuenta, prevenirse y eliminarse durante el ciclo de vida de los sistemas de IA para garantizar la seguridad y la protección de los seres humanos, del medio ambiente y de los ecosistemas.

  3. Equidad y no discriminación. Los actores de la Inteligencia artificial deben promover la diversidad y la inclusión, garantizar la justicia social, proteger la equidad y luchar contra todo tipo de discriminación, de conformidad con el derecho internacional. Los actores de la IA deberían hacer todo lo razonablemente posible por reducir al mínimo y evitar reforzar o perpetuar aplicaciones y resultados discriminatorios o sesgados a lo largo del ciclo de vida de los sistemas de IA, a fin de garantizar la equidad de dichos sistemas.

  4. Supervisión y decisión humanas. La decisión de ceder el control a los sistemas de IA en contextos limitados sigue siendo de los seres humanos, porque un sistema de IA no puede reemplazar la responsabilidad final de los seres humanos y su obligación de rendir cuentas.

  5. Transparencia y explicabilidad. La transparencia y la explicabilidad de los sistemas de IA son condiciones previas fundamentales para garantizar el respeto, la protección y la promoción de los derechos humanos, las libertades fundamentales y los principios éticos. Las personas deben tener la oportunidad de pedir explicaciones e información al responsable de la IA o a las instituciones del sector público correspondientes.

  6. Sensibilización y educación. La sensibilización y la comprensión del público sobre las tecnologías de IA y el valor de los datos deben promoverse mediante una educación abierta y accesible, la participación cívica, las competencias digitales y la capacitación en materia de ética del uso de la IA. También debe realizarse la alfabetización mediática e informacional y la capacitación dirigida conjuntamente por los gobiernos, las organizaciones intergubernamentales, la sociedad civil, las universidades, los medios de comunicación, los dirigentes comunitarios y el sector privado, para garantizar una participación pública efectiva.

Ademas el 26 de octubre de 2023 se presento el proyecto de ley "Marco legal para la regulación del desarrollo y uso de la Inteligencia Artificia". A continuacion vamos a poner los puntos que consideramos principales de este proyecto de ley:

### Artículo 4: Principios Éticos
 4. 1. Todo desarrollo y uso de IA debe estar basado en principios éticos fundamentales, incluyendo el respeto a la dignidad humana, la privacidad, la transparencia, la responsabilidad y la equidad.
 4. 2. Se prohíbe el uso de IA con fines ilegales, discriminatorios, maliciosos o que atenten contra los derechos humanos
### Artículo 5: Responsabilidad y Rendición de Cuentas 
 5. 1. Los desarrolladores, proveedores y usuarios de sistemas de inteligencia artificial son responsables de las consecuencias de sus acciones y decisiones relacionadas con el uso de la IA y deben garantizar que sus sistemas sean seguros, confiables y cumplan con los estándares de calidad establecidos. 
 5. 2. Se establece la obligación de documentar y divulgar el funcionamiento y los algoritmos utilizados en los sistemas de IA, para permitir la auditoría y evaluación de su impacto. 
 5. 3. Garantizar la calidad y la eficacia de los servicios ofrecidos con la inteligencia artificial Y Asumir la responsabilidad de las decisiones y acciones de los sistemas de inteligencia artificial.

### Artículo 6: Privacidad y Protección de Datos
 6. 1. Los sistemas de IA deben respetar y proteger la privacidad de los usuarios y el tratamiento de sus datos personales de acuerdo con la normativa vigente de protección de datos aplicables. 
 6. 2. En el marco de la ley 25326 protección de datos personales, se prohíbe el uso no autorizado de datos personales recopilados por sistemas de IA, y se establece la obligación de obtener el consentimiento informado de los individuos para el uso de sus datos. 
### Artículo 7: Transparencia y Explicabilidad 
 7. 1. Se requiere que los sistemas de IA sean transparentes en su funcionamiento, de manera que los usuarios comprendan cómo se toman las decisiones y se llega a los resultados. 
 7. 2. Se establece el derecho de los individuos a solicitar explicaciones de las decisiones tomadas por sistemas de IA que les afecten. 
### Artículo 8: Seguridad y Robustez
 8. 1. Los sistemas de IA deben ser diseñados y desarrollados con medidas de seguridad adecuadas para prevenir el acceso no autorizado, manipulación o interferencia malintencionada.
 8. 2. Se exige la implementación de salvaguardias técnicas y organizativas para garantizar la robustez y fiabilidad de los sistemas de IA.
### Artículo 11. Verificación y certificación. 
 11. 1 Los desarrolladores de sistemas de IA, en cualquier etapa de su ciclo de vida, deberán registrarlos conforme a los procedimientos previstos por el GACTEC (Gabinete Científico y Tecnológico) y en el marco de la ley 25.467 de Ciencia, Tecnología e Innovación. 
 11. 2 Los sistemas de inteligencia artificial deberán ser sometidos a un proceso de verificación y certificación que garantice su calidad y seguridad. Los responsables deberán acreditar que los sistemas cumplen con los requisitos técnicos y legales aplicables, así como con los principios éticos y las obligaciones establecidas en esta ley.
### Artículo 12: Considerar la prohibición y/o restricción de uso de los sistemas de IA en casos de: 
 12. 1 Violación de derechos humanos: cuando el sistema de inteligencia artificial es utilizado para violar los derechos humanos fundamentales, como la privacidad, la libertad de expresión, la igualdad o la dignidad humana. 
 12. 2 Discriminación injusta: cuando el sistema de inteligencia artificial es discriminatorio y produce resultados injustos o perjudiciales para ciertos grupos de personas, como la discriminación racial o de género. 
 12. 3 Daños graves o riesgos para la seguridad: cuando tenga potencial de causar daños graves a las personas, la sociedad o el medio ambiente.
 12. 4 Manipulación o influencia indebida: cuando es utilizado para manipular o influir de manera indebida en la toma de decisiones importantes, como en procesos electorales o en la formación de opiniones públicas, atentando la integridad y la legitimidad de dichos procesos.  
 12. 5 Falta de transparencia y rendición de cuentas: cuando un sistema de IA opera de manera opaca y no se puede auditar o comprender cómo toma sus decisiones y puede ser difícil evaluar su impacto y responsabilizar a los actores involucrados. En tales casos, una prohibición podría ser necesaria hasta que se establezcan mecanismos adecuados de transparencia y rendición de cuentas. 
 12. 6 En todos los casos que su uso genere la perpetuación de desigualdades o injusticias. 
 12. 7 La utilización de sistemas de inteligencia artificial en situaciones de emergencia deberá ser justificada y limitada en el tiempo.
### Artículo 22: Información y Educación sobre Uso Responsable 
 22. 1. Los desarrolladores, proveedores y usuarios de sistemas de inteligencia artificial deberán proporcionar información clara y comprensible sobre el uso responsable de los sistemas, incluyendo las limitaciones, riesgos y precauciones necesarias.
 22. 2. Se fomentará la educación y formación en el uso responsable de la inteligencia artificial, promoviendo la conciencia de los riesgos asociados y las buenas prácticas para evitar errores y minimizar los daños potenciales.
### Artículo 23: Denuncia de Errores de Uso 
 23. 1. Se establecerá un mecanismo de denuncia de errores de uso de la inteligencia artificial, donde las personas afectadas o cualquier persona que tenga conocimiento de un error de uso podrá informar a la autoridad competente. 
 23. 2. Las denuncias de errores de uso deberán ser tratadas de manera confidencial y se tomarán las medidas necesarias para investigar y corregir los errores reportados, así como para aplicar las sanciones correspondientes en caso de negligencia o incumplimiento.

### Normativas de la Union Europea con respecto a IA
Ley de IA: normas diferentes para niveles diferentes de riesgo
 La nueva normativa establece obligaciones para proveedores y usuarios en función del nivel de riesgo de la IA. Aunque muchos sistemas de IA plantean un riesgo mínimo, es necesario evaluarlos todos.

### Riesgo inaceptable
Los sistemas de IA de riesgo inaceptable son los que se consideran una amenaza para las personas y serán prohibidos. Incluyen:
 1. manipulación cognitiva del comportamiento de personas o grupos vulnerables específicos: por ejemplo, juguetes activados por voz que fomentan comportamientos peligrosos en los niños 
 2. puntuación social: clasificación de personas en función de su comportamiento, estatus socioeconómico o características personales 
 3. sistemas de identificación biométrica en tiempo real y a distancia, como el reconocimiento facial.
Aunque existen algunas excepciones a esta calificación. Por ejemplo, los sistemas de identificación biométrica a distancia "a posteriori", en los que la identificación se produce tras un retraso significativo, se permitirán para perseguir delitos graves y sólo cuando haya previa aprobación judicial.
### Alto riesgo
Los sistemas de IA que afecten negativamente a la seguridad o a los derechos fundamentales se considerarán de alto riesgo y se dividirán en dos categorías. 
 1. Los sistemas de IA que se utilicen en productos sujetos a la legislación de la UE sobre seguridad de los productos. Esto incluye juguetes, aviación, automóviles, dispositivos médicos y ascensores.
 2. Los sistemas de IA pertenecientes a ocho ámbitos específicos que deberán registrarse en una base de datos de la UE: 
* identificación biométrica y categorización de personas físicas 
* gestión y explotación de infraestructuras críticas 
* educación y formación profesional
* empleo, gestión de trabajadores y acceso al autoempleo 
* acceso y disfrute de servicios privados esenciales y servicios y prestaciones públicas 
* aplicación de la ley
* gestión de la migración, el asilo y el control de fronteras
* asistencia en la interpretación jurídica y aplicación de la ley.
Todos los sistemas de IA de alto riesgo serán evaluados antes de su comercialización y a lo largo de su ciclo de vida. Los ciudadanos y ciudadanas tendrán derecho a presentar reclamaciones sobre los sistemas de IA a autoridades nacionales específicas. 
Requisitos de transparencia
La IA generativa, como ChatGPT, no se considera de alto riesgo, pero tendrá que cumplir requisitos de transparencia y con la legislación de la UE en materia de derechos de autor:
* revelar que el contenido ha sido generado por IA 
* diseñar el modelo para evitar que genere contenidos ilegales
* publicar resúmenes de los datos protegidos por derechos de autor utilizados para el entrenamiento.

 Los modelos de IA de uso general que cuenten con un alto impacto y que pudieran plantear un riesgo sistémico, como el modelo de IA más avanzado GPT-4, tendrán que someterse a evaluaciones exhaustivas e informar a la Comisión de cualquier incidente grave. 
El contenido que haya sido generado o modificado con la ayuda de la IA, como imágenes, audio o vídeos (por ejemplo, las «ultrafalsificaciones»), tendrá que etiquetarse claramente como tal.





*Ejemplo de chatbot donde se aplica IA*

------------- A COMPLETAR ------------- 

#### Chatbot

*¿Que es?*
    
Un chatbot es un programa informático que utiliza inteligencia artificial (IA) y procesamiento del lenguaje natural (NLP) para comprender las preguntas de los clientes y automatizar las respuestas a dichas preguntas, simulando la conversación humana.

#### Tipos de chatbot
* **Dumb chatbot**: Tambien denominado Chatbot de ITR (Respuesta de interacción de texto), es aquel que funciona a través de una lógica secuencial, siguiendo los comandos simples que el programador introduce en él previamente. Se trata de la opción más básica y sólo es capaz de responder ante instrucciones muy específicas, es posible desarrollarlo de modo que proporcione respuestas del tipo conversacional, sacándolo de su rigidez. Puede utilizarse para responder FAQs/preguntas frecuentes o para complementar una landing page a modo de formulario.
* **Chatbot con tecnología word-spotting**: El chatbot de “Word-spotting” podría considerarse un híbrido entre los dumb chatbots y los basados en IA. Se denominan así porque funcionan “reconociendo palabras”, y en función de estas, dan una respuesta que ya está programada. Tiene un caracter mas conversacional que el dumb chatbot y a la vez quita la necesidad de un sistema basado en IA para comprender la pregunta del cliente. Por otro lado, una desventaja es que no considera la intención o el contexto al interpretar palabras clave, lo que ocasiona que sean menos precisos que los chatbots con IA.
* **Chatbot con inteligencia artificial**: Los chatbos con IA son aquellos que pueden entender las preguntas del usuario, sin importar como hayan sido redactadas. Cuando el chatbot no esta seguro de lo que la persona esta preguntando y encuentra mas de una accción que puede satisfacer el pedido, puede preguntar para esclarecer la pregunta. Inclusive, puede mostrar una lista de posibles acciones desde la cual el usuario puede seleccionar la opcion que mas se alinee con sus necesidades. Los algoritmos de machine learning que sustentan estos chatbots le permiten auto-aprender y desarrollar una base creciente de conocimiento inteligente sobre preguntas y respuestas que estan basadas en interacciones de usuario. En caso de usar deep-learning, mientras mas tiempo el chatbot este en funcionamento, de mejor forma podrá entender lo que el usuario quiere lograr y podrá proveer de respuestas mas detalladas y precisas.
* **Chatbot según el tipo de interacción**: Tenemos varios tipos de chatbots segun el tipo de interacción, que tienen como objetivo ayudar al negocio a crecer y proveer mejores servicios a los clientes, entre los que podemos encontrar:

  * **Chatbot de soporte**: Usado en servicio al cliente para completar cualquier tarea relacionada a dar soporte a clientes, como podria ser responder preguntas frecuentes, guiar a un usuario a lo largo de todo el proceso de realizar una demo o de un estadío de compra. Si el chatbot no puede satisfacer la solicitud, pasa al cliente a los agentes humanos de soporte.
  * **Chatbot de marketing y ventas**: Su proposito principal es ayudar a los clientes a explorar y comprar un producto o servicio, al enviarles informacion relevante o atractiva, intentando identificar potenciales clientes y guiandolos hasta un portal de ventas. 
  * **Chatbot de habilidades**: Son aquellos que pueden ejecutar comandos como "reproducir una cancion", "enviar un email", "hacer una llamada", etc. Para este tipo de chatbot, si se lo utiliza por medio de voz, permite que el usuario no necesita sostener el dispositivo o presionar algun boton para utilizarlo. De la misma forma, las ordenes pueden darse por medio de texto.
  * **Chatbot de entretenimiento**: Son utilizados usualmente para el único proposito de mejorar el compromiso del cliente con la marca y proveer una experiencia única y divertida a los usuarios. Suelen ser complejos de construir y suelen requerir de IA para desarrollarlos.


#### Objetivo/s a lograr con el chatbot

Nuestro objetivo es crear un chatbot de habilidades que se enfoque en la reproducción de musica en base a las preferencias del usuario, segun este lo indique. Por ejemplo, el usuario podría sugerir reproducir canciones de un género o canciones que no conozca, y el chatbot ir aprendiendo de estas elecciones gracias al uso de la IA para mas adelante reproducir canciones mas afines a los gustos del usuario.

#### Herramientas a utilizar

* Repositorio de código: Gitlab

* Almacenamiento de archivos/documentos: Drive y Gitlab

* Diagramas: Draw.io

* Administración de las tareas del proyecto: Jira

* Métodologia a utilizar: Métodologia agil Scrum 

### Fuentes

[¿Qué es el Onboarding Digital y como influye el sector financiero?](https://www.youtube.com/watch?v=KsyEjajwhxk)

[¿Qué es el Onboarding Digital?](https://www.mobbeel.com/blog/que-es-el-onboarding-digital/)

[Artificial Intelligence Can Enhance Customer Experience in Digital Onboarding](https://www.doxim.com/ai-can-enhance-customer-experience-in-digital-onboarding/)

[Extracción de datos y verificación de identidad con DNI y pasaportes](https://www.klippa.com/es/blog/informativo/verificacion-de-identidad/)
[Qué es un chatbot?](https://www.ibm.com/es-es/topics/chatbots)

[¿Qué es la biometría y cómo se utiliza en la seguridad?](https://latam.kaspersky.com/resource-center/definitions/biometrics)
[Tipos de chatbot: ventajas y características](https://centribal.com/es/tipos-de-chatbot-ventajas-y-caracteristicas/)

[La validación de identidad y su impacto en el onboarding digital](https://reconoserid.com/la-validacion-de-identidad-y-su-impacto-en-el-onboarding-digital/)

[¿Qué es AML (Anti Money Laundering) y cómo funciona el monitoreo de lavado de dinero?](https://blog.truora.com/es/aml)

[Digital Onboarding and Anti Money Laundering (AML)](https://fineksus.com/digital-onboarding-and-anti-money-laundering-aml/)

[Revoluciona tus conversaciones. Descubre los distintos tipos de Chatbots](https://blog.convertia.com/index.php/revoluciona-tus-conversaciones-descubre-los-distintos-tipos-de-chatbots/)

[10 Types of Chatbots: Key Specifics, Mechanics, and Areas of Usage](https://helpcrunch.com/blog/types-of-chatbots/#Types_of_chatbots_based_on_where_they_are_used)

[¿Cuáles son las nuevas directrices de la EBA sobre onboarding digital?](https://www.mobbeel.com/blog/eba-nuevas-directrices-sobre-soluciones-de-onboarding-digital/)

[Recomendaciones para el uso de Inteligencia Artificial](https://www.argentina.gob.ar/justicia/derechofacil/leysimple/educacion-ciencia-cultura/recomendaciones-para-el-uso-de)

[Proyecto de ley: Marco legal para la regulación del desarrollo y uso de la Inteligencia
Artificial](https://www4.hcdn.gob.ar/dependencias/dsecretaria/Periodo2023/PDF2023/TP2023/2505-D-2023.pdf)

[Ley de IA de la UE: primera normativa sobre inteligencia artificial](https://www.europarl.europa.eu/pdfs/news/expert/2023/6/story/20230601STO93804/20230601STO93804_es.pdf)

