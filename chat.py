# chat.py
import nltk 
from nltk.stem import WordNetLemmatizer
import pickle
import numpy as np
from songs_dataset import * 
from fingerprint_auth import *
from keras.models import load_model
import json
import random
from natural_language import *

nombre_chatbot = "Musichat"

lemmatizer = WordNetLemmatizer()
model = load_model('chatbot_model.h5')

intents = json.loads(open('intents.json').read())
words = pickle.load(open('words.pkl','rb'))
classes = pickle.load(open('classes.pkl','rb'))

#Se hizo una funcion para obtener el nombre del Chat bot
def get_chat_name():
    return nombre_chatbot

def clean_up_sentence(sentence):
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]
    return sentence_words

# return bag of words array: 0 or 1 for each word in the bag that exists in the sentence

def bow(sentence, words, show_details=True):
    # tokenize the pattern
    sentence_words = clean_up_sentence(sentence)
    # bag of words - matrix of N words, vocabulary matrix
    bag = [0]*len(words)
    for s in sentence_words:
        for i,w in enumerate(words):
            if w == s:
                # assign 1 if current word is in the vocabulary position
                bag[i] = 1
                if show_details:
                    print ("found in bag: %s" % w)
    return(np.array(bag))

def predict_class(sentence, model):
    # filter out predictions below a threshold
    p = bow(sentence, words,show_details=False)
    res = model.predict(np.array([p]))[0]
    ERROR_THRESHOLD = 0.25
    results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
    # sort by strength of probability
    results.sort(key=lambda x: x[1], reverse=True)
    return_list = []
    for r in results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
    return return_list

def getResponse(msg, ints, intents_json):
    tag = ints[0]['intent']
    list_of_intents = intents_json['intents']
    for i in list_of_intents:
        if(i['tag']== tag):
            result = getTagResponse(msg, i['tag'], i)
            break
    return result

def get_artist_from_user_input(user_input):
    artist = None
    for word in user_input.split():
        if word != 'del':
            artist =  get_artist_from_input(word)
            if artist != None:
                return artist
    return None

def get_song_year_from_user_input(user_input):
    for word in user_input.split():
        if str.isdigit(word):
            if exists_song_with_given_year(int(word)):
                return word
    return None

def get_genre_from_user_input(user_input):
    genre = None
    for word in user_input.split():
        genre = get_genre_from_input(word)
        if genre != None:
            return genre
        
    return None

def getTagResponse(msg, tag, intent):
    if tag == 'consulta_canciones_artista':
        if msg == '':
            return ['No ingresaste ningun artista', '']
        artist_requested = get_artist_from_user_input(msg)
        if artist_requested == None:
            return ["El artista que especificaste no existe", '']
        
        return ['Algunas canciones que compuso el artista ' + artist_requested + ' son: ', get_artist_songs_by_name(artist_requested)]
    if tag == 'consulta_canciones_mas_populares':
        return ['Por supuesto! Voy a listarte las canciones mas populares de los ultimos 20 años y sus artistas', get_most_popular_songs()]
    if tag == 'consulta_canciones_para_bailar':
        return ['Claro! Puedo recomendarte las 15 canciones mas bailables que conozco', get_most_danceable_songs()]
    if tag == 'year':
        if msg == '':
            return ['No ingresaste ningun año', '']
        song_year = get_song_year_from_user_input(msg)
        if song_year == None:
            return ['No se encontró una canción para el año ingresado, debes ingresar un año entre 1998 y 2020', '']
        
        return ['Comprendo!, aqui va una cancion al azar del año ' + song_year + ' que me pediste' ,get_random_song_by_year(song_year)]
    if tag == 'generos':
        if msg == '':
            return ['No ingresaste ningun genero', '']

        genre = get_genre_from_user_input(msg)
        if genre == None:
            return ['No se pudo encontrar cancion para el genero ingresado', '']
        
        song = get_random_song_by_genre(genre)
        return ['Entendido! Aqui va una cancion al azar del genero ' + genre + ' para ti', song]
    
    return [random.choice(intent['responses']), '']
        

def chatbot_response(msg):
    ints = predict_class(msg, model)
    res = getResponse(msg, ints, intents)
    return res

 


